#!/bin/bash

function downloadFile()
{
	echo "Paste URL link: "
	read url
	: 'filename=$(basename "$url")'
	wget -O countries $url
}


function findStrings(){
: '
this function is created for searching whole lines starting with 'P' Character.
' 
result2=$(grep -x '^[P]*P[^P]*$' countries)
echo "Result of query: "
echo "${result2^^}"

}

function typeWord()
{
	echo "Type in nation: "
	read nationName
	query=$(grep -iFx "[^. ]\+$nationName" countries)
	echo "Looking for $nationName in file"
	if grep -iFx "$nationName" countries; then
		echo "true"
	else
		echo "false"
	fi
	return $query
}


LOGFILE=/tmp/logfile.log
OUTFILE=/tmp/outfile.log

exec > >(tee -a $OUTFILE) 2> >(tee -a $LOGFILE >&2)
echo "$(date "+%m%d%Y %T") : Starting work"
downloadFile
findStrings
typeWord
edmff -s
echo "$(date "+%m%d%Y %T") : Done"

echo "Error log file saved to $LOGFILE"
echo "Output saved to $OUTFILE"