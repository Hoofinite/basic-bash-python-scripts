from asyncio.log import logger
import requests as req
import logging

logging.basicConfig(filename='/tmp/pythonlogfile.log', level=logging.DEBUG, 
                    format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger=logging.getLogger(__name__)

def getFile():
    URL = "https://gist.githubusercontent.com/kalinchernev/486393efcca01623b18d/raw/daa24c9fea66afb7d68f8d69f0c4b8eeb9406e83/countries"
    response = req.get(URL)
    open('countries', 'wb').write(response.content)


def findCountries():
    #this function role's to search for lines starting with "p" characters
    list1 = []
    with open("countries", "r") as file:
        for ln in file:
            if ln.startswith("P"):
                replacement = ln.replace('\n',"")
                list1.append(replacement)
    
    nations_list = list(map(lambda x: x.upper(), list1))
    print("Ordered lists of countries: ",*nations_list, sep="\n")
    file.close()
    
def checkNation():
    #this function job is to take the input from user 
    #and find the match with downloaded file 
    typedCountry = str(input("Type in the country:").lower())
    with open("countries","r") as file:
        file_output = file.read().lower()
        print("true") if typedCountry in file_output else print("false")
    file.close()
     
try:
    getFile()
    findCountries()
    checkNation()
    1/0 #used for error log message
except ZeroDivisionError as err:
    logger.error(err)